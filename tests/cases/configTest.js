
"use strict";

var appRootPath = require("app-root-path");
var Config      = require(appRootPath + "/lib");
var assert      = require("chai").assert;

describe("Config", function() {

  describe("instantiate without params", function () {

    it("should try to find appRootPath + /config and file called 'app'", function () {

      assert.throws(function() {
          let config = new Config();
        },
        "Unable to find configuration file: " + appRootPath + "/config/app.json"
      );

    });

  });

  describe("instantiate with directory", function () {

    it("should try to find file in passed directory and file called 'app'", function () {

      let config = new Config(false, appRootPath + "/tests/config");

      let actualConfig = {
        name: config.get("name"),
        age: config.get("age"),
        number: config.get("number")
      };

      let expectedConfig = {
        name: "app",
        age: 25,
        number: "0 3452 345 345"
      };

      assert.deepEqual(
        actualConfig,
        expectedConfig
      );

    });

  });

  describe("instantiate with directory and filename", function () {

    it("should try to find file in passed directory and passed file", function () {

      let config = new Config("test", appRootPath + "/tests/config");

      let actualConfig = {
        name: config.get("name"),
        age: config.get("age"),
        mobile: config.get("mobile")
      };

      let expectedConfig = {
        name: "test",
        age: 30,
        mobile: "0 1234 123 123"
      };

      assert.deepEqual(
        actualConfig,
        expectedConfig
      );

    });

  });

});
