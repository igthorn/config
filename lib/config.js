
"use strict";

var appRootPath = require("app-root-path");
var nconf       = require("nconf");
var fs          = require("fs");
var ConfigError = require("./configError");
var _           = require("underscore");

class Config
{
  constructor(configFilename, configDirectoryPath)
  {
    if (!configFilename) {
      configFilename = this.getConfigFilename();
    }

    if (!configDirectoryPath) {
      configDirectoryPath = this.getConfigDirectory();
    }

    let fullPathWithoutExtension = configDirectoryPath + "/" + configFilename;

    if (!fs.existsSync(fullPathWithoutExtension + ".json")) {
      throw new ConfigError(
        "Unable to find configuration file: " + fullPathWithoutExtension + ".json"
      );
    }

    nconf.add("env-file", {type: "file", file: fullPathWithoutExtension + ".env.json"});
    nconf.add("default-file", {type: "file", file: fullPathWithoutExtension + ".json"});
  }

  get(key)
  {
    return nconf.get(key);
  }

  getConfigFilename()
  {
    if (!this.configFilename) {
      this.configFilename = this.getEnvironment();
    }

    return this.configFilename;
  }

  setConfigFilename(configFilename)
  {
    if (!_.isString(configFilename)) {
      throw new ConfigError(
        "Invalid config file name passed. String expected"
      );
    }

    this.configFilename = configFilename;
    return this;
  }

  getConfigDirectory()
  {
    if (!this.configDirectory) {
      this.configDirectory = appRootPath + "/config";
    }

    return this.configDirectory;
  }

  setConfigDirectory(configDirectory)
  {
    if (!_.isString(configDirectory)) {
      throw new ConfigError(
        "Invalid config directory passed. String expected"
      );
    }

    this.configDirectory = configDirectory;
    return this;
  }

  getEnvironment()
  {
    nconf.argv().env("_");
    return nconf.get("NODE:ENV") || "app";
  }  
}

module.exports = Config;
