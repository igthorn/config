
## Igthorn Config

[![Igthorn](http://igthorn.com/assets/images/igthorn_resized.png "Igthorn")](http://igthorn.com "Igthorn")

Basic wrapper for loading configuration.

[![Build Status](https://gitlab.com/igthorn/config/badges/develop/build.svg)](https://gitlab.com/igthorn/config/builds)

Igthorn Config package provides smart config file loading mechanism allowing to use local/remote config overwrites(using [nconf](https://www.npmjs.com/package/nconf) package) without worry to commit them. This package is part of [Igthorn](http://igthorn.com "Igthorn") project.

### Usage

Typical usage would be to instantiate config(optionally passing filename and/or directory):

```
let Config = require("igthorn-config");
let config = new Config();
// this will load:
// appRootPath + "/config/app.json"
// and extend it with
// appRootPath + "/config/app.env.json"

// or
let Config = require("igthorn-config");
let config = new Config("test"); 
// this will load:
// appRootPath + "/config/test.json"
// and extend it with
// appRootPath + "/config/test.env.json"

// or
let appRootPath = require("app-root-path");
let Config = require("igthorn-config");
let config = new Config("main", appRootPath + "/settings");
// this will load:
// appRootPath + "/settings/main.json"
// and extend it with
// appRootPath + "/settings/main.env.json"
```

later ```config``` variable can be used in following way:
```
console.log(config.get("db:username"));
```

### Methods

Config class implements following methods:

* constructor(configFilename:string, configDirectoryPath:string) : self

  allows to pass custom config filename and/or config directory. Otherwise they are defaulting to:

  configFilename = "app" or NODE_ENV environment variable

  configDirectoryPath = appRootPath + "/config"

* get(key:string): mixed

  returns key of configuration matched via passed key name

## License

[MIT](license.md)
